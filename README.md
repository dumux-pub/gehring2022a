# Gehring2022a


Installation
============

To install this module, one may download and execute the file [installGehring2022a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/gehring2022a/-/blob/master/installGehring2022a.sh) in a new directory:
```
mkdir Gehring2022a
cd Gehring2022a
wget https://git.iws.uni-stuttgart.de/dumux-pub/gehring2022a/-/raw/master/installGehring2022a.sh
chmod +x installGehring2022a.sh
./installGehring2022a.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run. For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installGehring2022a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/gehring2022a/-/blob/master/installGehring2022a.sh).

Installation with Docker
=======================

The easiest way to install this module is to create a new directory and download the docker image:
```
mkdir Gehring2022a
cd Gehring2022a
wget https://git.iws.uni-stuttgart.de/dumux-pub/gehring2022a/-/raw/master/docker_gehring2022a.sh
```

After that, one may open the docker container by running
```
bash docker_gehring2022a.sh open
```

Applications
============

After the script has run successfully, all executables can be built

```bash
cd Gehring2022a/gehring2022a/build-cmake
make build_tests
```

and one may run them individually. They are located in the build-cmake folder according to the following path:

- appl/sanitizeGrid
- appl/pnmpermeabilityupscaling
- appl/2p/static

The main result can be executed with an input file e.g., by running

```bash
cd appl/2p/static
./test_pnm_2p_static test_pnm_2p_static.input
```
